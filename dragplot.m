% DRAGPLOT  Drag a plotted line from one figure into another.
%    Pressing shift while selecting for the drag will create a new figure
%    with plots of: Y1 vs Y2, X1 vs Y2, X2 vs Y1 and X1 vs X2.
%

% allows plotted lines to be dragged from one figure window and
% dropped into another.  Type DRAGPLOT to enable lines in the CURRENT
% figure window to be moved.  Press and hold the left mouse button to
% select a line object and drag it to the new figure window.  When you
% release the mouse button, the line will be moved.  It will be removed
% from the old window. If you hold down the CRTRL key while you drag and
% drop, the line object will be copied to the new window.  If you drop
% the line object outside of a figure window, it will be deleted. You
% can enable DRAGPLOT on as many open figure windows as you desire.
% The DRAGPLOT feature is suppressed while the figure window is in zoom
% mode.  It will be enabled when zoom is turned off.
%
% Example:
%   figure; plot(rand(1,30),'r'); dragplot
%   figure; plot(cos(2*pi*[0:32]/11),'b'); dragplot
% Now drag and drop the cosine curve or the random plot from one figure
% window to the other.

% NOTES:
% This script modifies the "WindowButtonDownFcn" and "WindowButtonUpFcn"
% properties, and creates an application data object named "dragplot" in
% the figure window to which it is applied.
%
% If the receiving figure contains subplots, the CURRENT subplot will
% receive the dragged plot.  If you desire to drop the plot into a
% different subplot, issue a SUBPLOT command to select the desired subplot
% before dragging and dropping.

% History 
% Version 2.0 - Doug Harriman (doug.harriman@hp.com)
%               Added capability to create an entirely new plot based on
%               drag line and target axes labels.
% Version 1.1 - Doug Harriman (doug.harriman@hp.com)
%               Restructured code into message switchyard.  
%               Changed functionality:
%               - Drag onto invalid target is a no-op instead of a delete.
%               - Drag onto valid target is always a copy operation.
%               - Restore all original figure properties on exit.
%               Subversion plottools revision 106.
% Version 1.0 - Mark W. Brown (mwbrown@ieee.org)
%               Provided functionality to drag line with copy or cut operation.
%               Subversion plottools revision 105.

function [] = dragplot(msg)

if nargin == 0,
    msg = 'Start';
end

switch lower(msg),

    case 'start',
        % Save out previous state
        oldDwnFcn = get(gcf,'WindowButtonDownFcn');
        oldUpFcn  = get(gcf,'WindowButtonUpFcn');
        setappdata(gcf,'DragPlotDwnFcn',oldDwnFcn);
        setappdata(gcf,'DragPlotUpFcn',oldUpFcn);

        % Set the new btn fcns.
        set(gcf,'WindowButtonDownFcn','dragplot(''StartDrag'');');
        set(gcf,'WindowButtonUpFcn','dragplot(''StopDrag'');');
        
    case 'startdrag',
        % Check for line type object and change cursor and selection state.
        objType = get(gco,'Type');

        % Bail if not a line object.
        if ~strcmp(objType,'line'), return; end

        % User selected a line
        % Toggle pointer & store out
        oldPnt = get(gcf,'Pointer');
        setappdata(0,'DragPlotPointer',oldPnt);
        set(gcf,'Pointer','Fleur');

        % Set line to selected
        set(gco,'Selected','On');

        % Store out selection handle
        setappdata(gcf,'DragPlotObjHan',gco);

    case 'stopdrag',
        % Get handle of figure window drag operation ended within.
        %newFig = get(0,'PointerWindow');
        newFig = pointerwindow;

        % Restore original figure properties
        % Note that since we haven't actually clicked in the new figure
        % window, that the 'current figure' value is still the original
        % figure.
        oldDwnFcn = getappdata(gcf,'DragPlotDwnFcn');
        oldUpFcn  = getappdata(gcf,'DragPlotUpFcn');
        oldPnt    = getappdata(0,'DragPlotPointer');
        set(gcf,'WindowButtonDownFcn',oldDwnFcn);
        set(gcf,'WindowButtonUpFcn',oldUpFcn);
        set(gcf,'Pointer',oldPnt);
        
        % Get the line's selection state
        % Original dragplot.m also check that 'selected' property was 'on'.
        lineHan = getappdata(gcf,'DragPlotObjHan');
        selState = get(gcf,'SelectionType');
        
        % Turn off selection
        set(lineHan,'Selected','Off');
        
        % Decide if we're in a valid figure to drop the line
        if newFig <= 0,
            % We're not, so bail.
            % Effectively a no-op.  This is different than the Mark Browns
            % implementation.  He would delete the object if the drag
            % target was not valid.
            return;
        end
        
        % Look at axes labels to see if we want to generate an entirely new
        % plot.  In order to generate a new plot, the axes labels must not
        % be empty and match exactly.
        newAx   = get(newFig,'CurrentAxes');
        oldXLabel = get(get(gca,'xLabel'),'String');
        oldYLabel = get(get(gca,'yLabel'),'String');
        newXLabel = get(get(newAx,'xLabel'),'String');
        newYLabel = get(get(newAx,'ylabel'),'String');
        
        % Get selection type
        selType = get(gcf,'selectiontype');
        
        if ~strcmp(selType,'extend'),
            % Copy the line object to the new axes
            lineHan = copyobj(lineHan,newAx);
            axes(newAx);
            pt;
            return;
        end
       
        % Should give user option of just adding a line to the plot as
        % above, or creating 1 of the four possible new plots.
        % Good way might be to use 'alt' or 'ctrl' to do the new plot
        % creation, normal to do the copy.
        % Need to handle:
        %  - Multiple traces in the target window.
        %  - Data that needs to be interpolated between the windows.
        % For now do the simple case and use the first trace in the target
        % window and hope that no interpolation is necessary.
        
        % Get the data from the two lines
        oldX = get(lineHan,'xData');
        oldY = get(lineHan,'yData');
        lineHan2 = findobj(get(newAx,'Children'),'Type','Line');
        lineHan2 = flipud(lineHan2);
        lineHan2 = lineHan2(1);
        newX = get(lineHan2,'xData');
        newY = get(lineHan2,'yData');

        % Try to do right thing if data vectors are different lengths.
        if length(oldX) ~= length(newX),
            newXLabel = get(get(newAx,'XLabel'),'String');
            newYLabel = get(get(newAx,'YLabel'),'String');
            oldXLabel = get(get(gca,'XLabel'),'String');
            oldYLabel = get(get(gca,'YLabel'),'String');
            
            % Look for a match.
            if strcmp(newXLabel,oldXLabel),
                % X's match so can proceed.
            elseif strcmp(newXLabel,oldYLabel),
                % Swap new X<->Y, then proceed.
                temp = newY;
                newY = newX;
                newX = temp;
            else
                error('Plot data vectors are different lengths and plot labels do not match.');
            end
            
            % Make sure X data values are distinct
            [tmp,idx] = unique(newX);
            idx  = sort(idx);
            newX = newX(idx);

            [tmp,idx] = unique(oldX);
            idx  = sort(idx);
            oldX = oldX(idx);
            
            
            % Reduce the X ranges to the minimum common set.
            minX = max(min(newX),min(oldX));
            maxX = min(max(newX),max(oldX));
           
            oldIdx = (oldX>=minX) & (oldX<=maxX);
            newIdx = (newX>=minX) & (newX<=maxX);
            
            newX = newX(newIdx);
            newY = newY(newIdx);
            oldX = oldX(oldIdx);
            oldY = oldY(oldIdx);
            
            % Do interpolation.
            X = union(newX,oldX);
            oldY = interp1(oldX,oldY,X);
            newY = interp1(newX,newY,X);
            
            oldX = X;
            newX = X;
        end
        
        oldFig = gcf;
        figure;
        subplot(2,2,1);plot(oldY,newY); xlabel(oldYLabel);ylabel(newYLabel);grid;
        subplot(2,2,2);plot(oldX,newY); xlabel(oldXLabel);ylabel(newYLabel);grid;
        subplot(2,2,3);plot(newX,oldY); xlabel(newXLabel);ylabel(oldYLabel);grid;
        subplot(2,2,4);plot(oldX,newX); xlabel(oldXLabel);ylabel(newXLabel);grid;
        
        suptitle('Select axes set to keep');
        set(gcf,'WindowButtonDownFcn','dragplot(''SelectAxes'');');
        
    case 'selectaxes'
        axHan  = gca;
        axList = findobj(get(gcf,'Children'),'type','axes');
        delete(setdiff(axList,axHan));
        set(axHan,'Position',get(0,'DefaultAxesPosition'));
        pt
        
    otherwise,
        error(['Unknown message: ' upper(msg)]);
end
