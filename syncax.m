%SYNCAX  Syncronizes the axes of all figures to the current axes limits.
%   SYNCAX(AXES) syncronizes the specified axes.  AXES is optional and 
%   may be 'x','y' or 'xy'.  The default value is 'xy'.
%
%   Written by: Doug Harriman (doug_harriman@comcast.net)
%

% DLH
% 08/03/00 - Created

function [] = syncax(msg)

% Error check input
if nargin == 0,
   msg = 'xy';
elseif nargin == 1,
   msg = lower(msg);
else
   error('SYNCAX requires only 1 input.');
end

% Figures to deal with
figList = get(0,'children');

% Axes to extract limits from
curAx = gca;
xLim  = get(curAx,'xlim');
yLim  = get(curAx,'ylim');

% Get list of all other axes.
axList = [];
for fig = 1:length(figList),
   axList = [axList findobj(get(figList(fig),'children'),'type','axes')'];	    %#ok<AGROW>
end

% Drop any legend axes.
tag = get(axList,'tag');
idx = ~strcmp(tag,'legend'); % Find all non-legend axes.
axList = axList(idx);        % Keep only those.

% Set the limits
switch msg
case 'x'
   set(axList,'xlim',xLim);
case 'y'
   set(axList,'ylim',yLim);
case 'xy'
   set(axList,'xlim',xLim);
   set(axList,'ylim',yLim);
otherwise
   error('Unknown axes');
end
