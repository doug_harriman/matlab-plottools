%STRETCH   Sets x-axis limits to limits of data of first line object.
%
%   Written by: Doug Harriman (doug_harriman@comcast.net)
% 

% DLH
% 05/15/98 - Created

function [] = stretch(axhan)

% error check
if nargin==0,
   axhan = gca ;
end

% Get max x point
linehan = get(axhan,'children') ;
linehan = linehan(1) ;
data = get(linehan,'xdata') ;

% Set the limit
set(axhan,'xlim',[min(data) max(data)]) ;



