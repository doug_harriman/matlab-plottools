%ANIMOBJ   Graphics object property animation manager.
%   ANIMOBJ provides a simple infrastructure to manage the animation of any
%   property of a Matlab handle graphics object.  ANIMOBJ works by
%   registering graphics object handles, then registering the properties of
%   those objects that should be animated.  The default animation behavior
%   is linear interpolation between two numeric property values.  User
%   supplied animation behavior functions are also supported.  ANIMOBJ is
%   configured by passing various messages and parameters.  
%
%   The supported message are:
%
%    NewAnimation       - Clears out animation data to prepare for a new animation.
%    AddObject          - Adds a handle graphics object to the animation list.
%                         ANIMOBJ('AddObject',HANDLE)
%    RmObject           - Removes a handle graphics object from the animation list.
%                         ANIMOBJ('RmObject',HANDLE)
%    SetAnimateProperty - Defines the animation behavior for a property of an
%                         object in the animation list.
%                         ANIMOBJ('SetAnimateProperty',HANDLE,PROPERTY,INITIAL_VALUE,FINAL_VALUE,UPDATE_FCN)
%                         See Animate Property section below.
%    SetTotalFrames     - Sets the total number of frames in the animation.
%    GetTotalFrames     - Returns the total number of frames in the
%                         animation.
%    Animate            - Runs the animation from the current frame.  If the current 
%                         frame is equal to the total number of frames, the
%                         animation is restarted.
%    Step               - Advance the animation by one frame.
%    SetCurrentFrame    - Sets the animation to the current frame.  Does
%                         this by running animation to the desired frame.
%    GetCurrentFrame    - Returns the current frame number.
%    SetFrameCaptureFcn - Sets a function to be called to capture each
%                         frame of an animation.  A function handle or
%                         function name to call must be provided.  See the
%                         Frame Capture Function section below.
%    GetFrameCaptureFcn - Returns the frame capture function.
%
%
%   Set Animate Property Detail
%   ---------------------------
%   The most important message for configuration of animation is the
%   'SetAnimateProperty' message.  Calling with this message takes the
%   following form:
%
%   ANIMOBJ('SetAnimateProperty',HANDLE,PROPERTY,INITIAL_VALUE,FINAL_VALUE,UPDATE_FCN)
%
%   The inputs are:
%   HANDLE        - Handle of graphics object for which to animate the
%                   property.  Required input.
%   PROPERTY      - Property to animate.  Required input.
%   INITIAL_VALUE - Initial value of the property at frame 1.  If using the
%                   default animation behavior function this must be a
%                   numeric value.  Required input.
%   FINAL_VALUE   - Final value of the property at the last frame.
%                   Required input.
%   UPDATE_FCN    - Optional input.  This input specifies a custom
%                   animation behavior function.  The input may be either a
%                   function handle or a character string name of the
%                   function to call.  The function will recieve a data
%                   structure with the following fields:
%
%                   .handle        - Handle of the object to animate.
%                   .property      - Property to animate.
%                   .value_initial - Initial value of the property.
%                   .value_final   - Final value of the property.
%                   .value_current - Current value of the property.
%                   .frame_total   - Total number of frames in the animation.
%                   .frame_current - Current frame in the animation.
%
%                   The function should return the updated value of the
%                   property.
%
%
%   Frame Capture Function
%   ----------------------
%   The frame capture function allows the user to specify a function to
%   capture and record animation frames for later playback.  The function
%   should have the following signature:
%
%   [] = function function_name(current_frame,total_frames)
%
%   
%   Usage example
%   -------------
%   animobj('NewAnimation');  % Reset all internal data
%   h = plot(0.5,0.5,'ro');   % Base circle, animate its size.
%   animobj('AddObject',h);   % Add this object to the list of those to animate.
%   animobj('SetAnimateProperty',h,'MarkerSize',1,10);  % Animate the size
%                             % of the circle from 1 to 10.
%   animobj('SetTotalFrames',10); %  Ten total frames.
%   animobj('Animate');       % Do the animation.
%
%   See also: movie, getframe, print.
%

% Internal data documentation.
%
% All internal state data is stored in an appdata field.
%
% The main data structure handles the general animation properties:
% .handles           - Vector of object handles
% .frame_total       - Number of frames in animation
% .frame_current     - Current frame in animation.
% .objects           - Vector of object structures.
% .frame_capture_fcn - User specified function to capture animation frames.
%
% The main data structure field ".objects" is an array of structures with
% two fields:
% .handle   - Copy of this objects handle.
% .property - Structure of fields that are animated.
%
% The ".property" field is a structure with fields that are named for the
% properties that are set for animation.  Each of these fields is in turn a
% structure with the following fields:
%
% .value_initial       - Initial value of this property
% .value_final         - Final   value of this property
% .value_current       - Current value of this property
% .property_update_fcn - Function handle to a animobj update function.
%

% Doug Harriman (doug.harriman@comcast.net)
% 11/16/05 - Created

function [varargout] = animobj(msg,varargin)

% Constants
FIELD = 'AnimateObjectData';

% Get state data 
if ~strcmp(lower(msg),'newanimation') & ~isappdata(0,FIELD),
    animobj('NewAnimation');
    data = getappdata(0,FIELD);
else
    data = getappdata(0,FIELD);
end

% Handle input.
switch(lower(msg)),
    case 'newanimation',
        % Error check inputs.
        if nargin > 1, error('One input expected.'); end
        
        % Create a new animation.  Clear out all old animation data.
        data = struct();
        data.handles       = [];
        data.frame_total   = 1;
        data.frame_current = 1;
        data.frame_capture_fcn = @DefaultFrameCaptureFcn;
        
    case 'addobject',
        % Error check input.
        han = varargin{1};
        if ~all(ishandle(han)),
            error('Input must be valid handle graphics object.');
        end
        
        % Allow vector of handles to be passed in at once.
        if length(han) > 1,
            for i = 1:numel(han),
                % Call this function for each element.
                animobj('AddObject',han(i));
            end
            return;
        end
        
        % If we already have object, just exit.
        if ~isempty(find(han==data.handles)),
            return;
        end
        
        % Store out the default data for the new object 
        index = length(data.handles) + 1;
        data.handles(index) = han;

        % Create an empty new object.
        newobj.handle        = han;
        newobj.property      = struct();
        data.objects(index)  = newobj;
        
    case 'rmobject',
        % Error check input.
        han   = varargin{1};
        if ~all(ishandle(han)),
            error('Input must be valid handle graphics object.');
        end
        
        % Allow vector of handles to be passed in at once.
        if length(han) > 1,
            for i = 1:numel(han),
                % Call this function for each element.
                animobj('RmObject',han(i));
            end
            return;
        end

        % If we don't have that object stored, bail out.
        if isempty(find(han==data.handles)),
            return;
        end

        % Delete the handle and object from their arrays.
        index = find(data.handles==han);
        keep_handles = setdiff([1:length(data.handles)],index);
        data.handles(index) = [];
        data.objects = data.objects(keep_handles);
         
    case 'setanimateproperty',
        % Error check inputs.
        if(nargin < 4),
            error('Must specify handle, property, intial value and final value.  See help animobj.');
        end
                
        han = varargin{1};
        if ~all(ishandle(han)),
            error('Input must be valid handle graphics object.');
        end

        % Allow vector of handles to be passed in at once.
        if length(han) > 1,
            for i = 1:numel(han),
                % Call this function for each element.
                animobj('SetAnimateProperty',han(i),varargin{2:end});
            end
            return;
        end

        % Make sure user has already registered this object.
        index = find(data.handles==han); 
        if isempty(index),
            error('Handle not in animation object list.  Use animobj(''AddObject'',<handle>) first.');
        end
        
        % See if the property exists for this object.
        property = lower(varargin{2});
        if ~isstr(property), error('Property specification must be as string.'); end
        try,
            get(han,property);
        catch,
            warning(['Object has no property: ' property]);
        end
        
        % No error checking on the values.  Could be anything for a user
        % specified function.
        value_initial = varargin{3};
        value_final   = varargin{4};
        if length(varargin) > 4,
            update_fcn    = varargin{5};
            ValidateFcn(update_fcn);
        else,
            update_fcn    = @DefaultAnimationPropertyUpdateFcn;
        end
    
        % Store out animation property data
        property_data.value_initial = value_initial;
        property_data.value_final   = value_final;
        property_data.value_current = value_initial;
        property_data.property_update_fcn = update_fcn;
        
        data.objects(index).property = setfield(data.objects(index).property,...
            property,property_data);

    case 'getanimateproperty',
        error(['Message not implemented yet: ' upper(msg)]);
        
    case 'setframecapturefcn',
        % Error check inputs.
        if nargin ~= 2, error('Two inputs expected.'); end
        
        % Handle either fcn handle or string.
        fcn = varargin{1};
        ValidateFcn(fcn);
        data.frame_capture_fcn = fcn;
        
    case 'getframecapturefcn',
        % Error check inputs
        if nargin ~= 1, error('One input expected.'); end
        
        varargout{1} = data.frame_capture_fcn;
        
    case 'settotalframes', 
        % Error check inputs
        if nargin ~= 2, error('Two inputs expected'); end

        n_frames = varargin{1};
        if ~isa(n_frames,'double'), error('Frame count specification should be a double.'); end
        if ~isscalar(n_frames), error('Frame count specification should be scalar.');   end
        
        data.frame_total = n_frames;
        
    case 'gettotalframes',
        % Error check inputs
        if nargin ~= 1, error('One input expected.'); end

        varargout{1} = data.frame_total;
        
    case 'setcurrentframe', 
        % Error check input
        if nargin ~= 2, error('Two inputs expected.'); end
        frame = varargin{1};
        if numel(frame) ~= 1, error('Frame specification must be scalar'); end
        if ~isnumeric(frame), error('Frame specification must be numeric.'); end
        frame = round(frame);
        if frame > data.frame_total,
            error(['Animation only has ' num2str(data.frame_total) ' frames.']);
        end
        
        % Restart animation and run to specified frame.
        data.frame_current = 1;
        setappdata(0,FIELD,data);
        for i = 1:frame,
            animobj('Step');
        end
        data = getappdata(0,FIELD);  % So we don't overwrite with our current, out of date data.
        
    case 'getcurrentframe',
        % Error check inputs
        if nargin ~= 1, error('One input expected.'); end

        varargout{1} = data.frame_current;
        
    case 'step',
        % Error check inputs
        if nargin ~= 1, error('One input expected.'); end

        % Restart if at end
        if(data.frame_current > data.frame_total),
            data.frame_current = 1;
        end
        
        % Loop through objects updating properties.
        n_obj = length(data.handles);
        for i_obj = 1:n_obj;
            % Updated each object and each property for that object.
            handle         = data.handles(i_obj);
            prop_list_data = data.objects(i_obj).property;

            prop_list_names = fieldnames(prop_list_data);
            for i_prop = 1:length(prop_list_names),
                % Property that we're working on.
                prop_name = prop_list_names{i_prop};
                
                % Extract the field to update
                prop_struct = getfield(prop_list_data,prop_name);
                
                % Build property data structure to pass to property update
                % fcn.
                update.handle        = handle;
                update.property      = prop_name;
                update.value_initial = prop_struct.value_initial;
                update.value_final   = prop_struct.value_final;
                update.value_current = prop_struct.value_current;
                update.frame_total   = data.frame_total;
                update.frame_current = data.frame_current;
                
                % Handle first frame state reset
                if(data.frame_current == 1),
                    update.value_current = prop_struct.value_initial;
                end
                
                % Call the property update fcn.
                fcn = prop_struct.property_update_fcn;
                if ischar(fcn),
                    value_current = feval(fcn,update);
                else,
                    value_current = fcn(update);
                end

                % Update the property
                try, set(handle,prop_name,value_current); end
                    
                % Save the state
                prop_struct.value_current = value_current;
                data.objects(i_obj).property = setfield(data.objects(i_obj).property,...
                    prop_name,prop_struct);
            end
        end

        % Update the frame count.
        data.frame_current = data.frame_current+1;
        
    case 'animate'
        % Error check inputs
        if nargin ~= 1, error('One input expected.'); end

        % Runs the animation from the current frame.
        frame_start = data.frame_current;

        % Restart animation if we're at the end.
        if (frame_start >= data.frame_total),
            frame_start = 1;
        end
        
        % Loop through the frames
        for i_step = frame_start:data.frame_total,

            % Advance the animation and force a drawing update
            animobj('Step');
            data = getappdata(0,FIELD);
            drawnow;
            
            % Call the frame capture fcn.
            % Current frame number has been pre-incremented.
            fcn = data.frame_capture_fcn;
            if ischar(fcn),
                feval(fcn,data.frame_current-1,data.frame_total);
            else,
                data.frame_capture_fcn(data.frame_current-1,data.frame_total);
            end
        end
        
    otherwise
        error(['Unknown message: ' msg]);
end

% Store out any changes to the state data
setappdata(0,FIELD,data);

end 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Sub Functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%--------------------------------------------------------------------------
% DefaultAnimationPropertyUpdateFcn
% Default animation function. Linear interpolation between numeric values.
%--------------------------------------------------------------------------
function [value_new] = DefaultAnimationPropertyUpdateFcn(data)

% Calculate update per frame
d_value = (data.value_final-data.value_initial)/data.frame_total;

% Calculate new value
value_new = data.value_current + d_value;

end

%--------------------------------------------------------------------------
% DefaultFrameCaptureFcn
% 100ms delay between frames.
%--------------------------------------------------------------------------
function [] = DefaultFrameCaptureFcn(current_frame,total_frames)
pause(0.1);
end

%--------------------------------------------------------------------------
% ValidateFcn
% Validates a user specified function.
% Throws error if function specifier is invalid.
%--------------------------------------------------------------------------
function [] = ValidateFcn(fcn)

switch(class(fcn)),
    case 'function_handle',
        % Do nothing, fcn is valid.
    case 'char',
        % Make sure that we can find the function.
        ex = exist(fcn);
        ind = find(ex == [2 3 5 6]);
        if isempty(ind),
            error(['Unable to find function: ' fcn]);
        end
    otherwise,
        error('User specified function must be either a function handle or string.');
end

end % function