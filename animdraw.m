%ANIMDRAW  Example function to do a custom animation.
% 
%   See also: animobj, animtest1.
%

% Doug Harriman (doug.harriman@comcast.net)

function [val] = animdraw(data)

% Extract the data from the properties initial value.
total_pts = length(data.value_initial.x);
xdata = data.value_initial.x;
ydata = data.value_initial.y;

% Figure out what to plot
xdata = xdata(1:total_pts-data.frame_current);
ydata = ydata(1:total_pts-data.frame_current);

% Do the plot
set(data.handle,'XData',xdata,'YData',ydata);

% Animation functions must return a value, even if it is empty.
val = [];