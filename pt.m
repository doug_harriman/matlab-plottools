%PT  Plot Tools.
%

% DLH
% 12/??/98 - Created

function [] = pt(msg,varargin)

% Creation check
if nargin == 0,
    msg = gcf;
end
if ~ischar(msg)
    % Initialize axesHandle
    axesHandle = [];
    
    % Loop through vector of handles  NOT HANDLING MULTIPLE FIGURES OR ROOT PROPERLY
    for i = 1:length(msg),
        % See what type of handle we have
        switch(get(msg(i),'Type')),
            case 'figure'
                % Force figure on screen
%                 onscreen;

                children = get(msg(i),'Children');
                children = findobj(children,'Type','axes');
                pt(children);
            case 'root'
                children = get(msg(i),'Children');
                children = findobj(children,'Type','figure');
                pt(children);
            case 'axes'
                axesHandle = [axesHandle msg(i)];
                grid('on');
                box('on');
        end
    end
    
    % Set msg
    msg = 'create';
    
else
    axesHandle = gca;
end

% Message Switchyard
switch lower(msg)
    case 'create'
        
        for i = 1:length(axesHandle),
            
            % Associate string editing with x,y,&z labels, and title.
            set(get(axesHandle(i),'XLabel'),'ButtonDownFcn','pt(''StringEdit'',''xlabel'');');
            set(get(axesHandle(i),'YLabel'),'ButtonDownFcn','pt(''StringEdit'',''ylabel'');');
            set(get(axesHandle(i),'ZLabel'),'ButtonDownFcn','pt(''StringEdit'',''zlabel'');');
            set(get(axesHandle(i),'Title'),'ButtonDownFcn','pt(''StringEdit'',''title'');');
            
            % Axes limit editing.
            
            % Axes Context menu
            contextMenu = uicontextmenu('Tag','ptAxesContextMenu',...
                'CallBack','pt(''ptAxesContextMenuSelected'');');
            
            axesMenu = contextMenu;
            uimenu(axesMenu,'Label','Axes Limits',...
                'Callback',@AxesEdit);
            
            uimenu(axesMenu,'Label','Dynamic Zoom',...
                'Separator','On',...
                'Callback','dzoom');
            uimenu(axesMenu,'Label','Stretch Plot',...
                'Callback','stretch(gca);');
            uimenu(axesMenu,'Label','Fit Plot',...
                'Callback',...
                'set(gca,''YLimMode'',''Auto'',''XLimMode'',''Auto'');');
            syncMenu = uimenu(axesMenu,'Label','Syncronize Axes');
            
            uimenu(axesMenu,'Label','Push Axis',...
                'Separator','On',...
                'Callback','pushaxis');
            popMenu = uimenu(axesMenu,'Label','Pop Axis');
            uimenu(popMenu,'Label','XY',...
                'Callback','popaxis(''xy'')');
            uimenu(popMenu,'Label','X',...
                'Callback','popaxis(''x'')');
            uimenu(popMenu,'Label','Y',...
                'Callback','popaxis(''y'')');
            
            
            uimenu(axesMenu,'Label','Hold',...
                'Separator','On',...
                'Callback','pt(''HoldToggle'');',...
                'Tag','ToggleHoldMenu');
            uimenu(axesMenu,'Label','Swap X<->Y',...
                'Callback','flipxy;');
            uimenu(axesMenu,'Label','Flip Y Direction',...
                'Callback','flipy;');
            uimenu(axesMenu,'Label','Clear',...
                'Callback','clf');
            uimenu(axesMenu,'Label','PSD',...
                'Separator','On',...
                'Callback','psdplot;',...
                'Tag','PSD');
            uimenu(axesMenu,'Label','Time Windowed Avg',...
                'Callback',@TimeWindowAvg,...
                'Tag','TimeWindowAvg');
            uimenu(axesMenu,'Label','Dragplot',...
                'Separator','On',...
                'Callback','dragplot;pt;',...
                'Tag','dragplot');
            uimenu(axesMenu,'Label','Data Table',...
                'Separator','On',...
                'Callback','gtable;');
            
            % Add cursors if dualcursor.m is installed.
            if exist('dualcursor'),
                uimenu(axesMenu,'Label','Cursors',...
                    'Separator','On',...
                    'Callback','dualcursor;');
            end
            
            % Associate it with the axes
            set(axesHandle(i),'UIContextMenu',contextMenu);
            
            % Data Context menu
            contextMenu = uicontextmenu('Tag','ptLineContextMenu',...
                'Callback','pt(''LineContextMenu'');');
            uimenu(contextMenu,'Label','Name:',...
                'Tag','LineMenuName');
            
            uimenu(contextMenu,'Label','Meta Data',...
                'Visible','Off',...
                'Separator','On',...
                'Tag','LineMenuMeta');
            uimenu(contextMenu,'Label','X:',...
                'Separator','On',...
                'Tag','LineMenuX');
            uimenu(contextMenu,'Label','Y:',...
                'Tag','LineMenuY');
            uimenu(contextMenu,'Label','Ind:',...
                'Tag','LineMenuInd');
            
            subContextMenu = uimenu(contextMenu,'Label','Label',...
                'Separator','On');
            uimenu(subContextMenu,'Label','X',...
                'Tag','LineMenuMarkX');
            uimenu(subContextMenu,'Label','Y',...
                'Tag','LineMenuMarkY');
            uimenu(subContextMenu,'Label','X,Y',...
                'Tag','LineMenuMarkXY');
            uimenu(subContextMenu,'Label','Text',...
                'Tag','LineMenuMarkText');
            uimenu(subContextMenu,'Label','Meta Title',...
                'Separator','On',...
                'Callback','pt(''metatitle'');',...
                'Tag','MetaTitle');
            
            % Statistics Submenu
            statContextMenu = uimenu(contextMenu,'Label','Statistics');
            uimenu(statContextMenu,'Label','Samples',...
                'Callback','pt(''StatCallback'');',...
                'Tag','StatMenuSamples');
            uimenu(statContextMenu,'Label','Mean',...
                'Callback','pt(''StatCallback'');',...
                'Tag','StatMenuMean');
            uimenu(statContextMenu,'Label','RMS',...
                'Callback','pt(''StatCallback'');',...
                'Tag','StatMenuRMS');
            uimenu(statContextMenu,'Label','Standard Deviation',...
                'Callback','pt(''StatCallback'');',...
                'Tag','StatMenuDev');
            uimenu(statContextMenu,'Label','Maximum',...
                'Callback','pt(''StatCallback'');',...
                'Tag','StatMenuMax');
            uimenu(statContextMenu,'Label','Minimum',...
                'Callback','pt(''StatCallback'');',...
                'Tag','StatMenuMin');
            uimenu(statContextMenu,'Label','Histogram',...
                'Callback','pt(''Histogram'');',...
                'Tag','StatHistMenu');
            uimenu(statContextMenu,'Label','Label',...
                'Separator','On',...
                'Checked','On',...
                'Callback','pt(''StatToggle'');',...
                'Tag','StatMenuMark');
            uimenu(statContextMenu,'Label','To Workspace',...
                'Checked','On',...
                'Callback','pt(''StatToggle'');',...
                'Tag','StatMenuWorkspace');
            
            % Marker Submenu
            markerContextMenu = uimenu(contextMenu,'Label','Marker');
            uimenu(markerContextMenu,'Label','None',...
                'Callback','set(gco,''Marker'',''None'');pt(''DeletePointMarker'');');
            uimenu(markerContextMenu,'Label','o',...
                'Callback','set(gco,''Marker'',''o'');pt(''DeletePointMarker'');');
            uimenu(markerContextMenu,'Label','+',...
                'Callback','set(gco,''Marker'',''+'');pt(''DeletePointMarker'');');
            uimenu(markerContextMenu,'Label','*',...
                'Callback','set(gco,''Marker'',''*'');pt(''DeletePointMarker'');');
            uimenu(markerContextMenu,'Label','.',...
                'Callback','set(gco,''Marker'',''.'');pt(''DeletePointMarker'');');
            uimenu(markerContextMenu,'Label','x',...
                'Callback','set(gco,''Marker'',''x'');pt(''DeletePointMarker'');');
            uimenu(markerContextMenu,'Label','Square',...
                'Callback','set(gco,''Marker'',''Square'');pt(''DeletePointMarker'');');
            uimenu(markerContextMenu,'Label','Diamond',...
                'Callback','set(gco,''Marker'',''Diamond'');pt(''DeletePointMarker'');');
            uimenu(markerContextMenu,'Label','Triangle',...
                'Callback','set(gco,''Marker'',''^'');pt(''DeletePointMarker'');');
            
            % Synchronization submenu
            uimenu(syncMenu,'Label','X',...
                'Callback','syncax x;');
            uimenu(syncMenu,'Label','Y',...
                'Callback','syncax y;');
            uimenu(syncMenu,'Label','XY',...
                'Callback','syncax;');
            
            % Color option
            uimenu(contextMenu,'Label','Color ...',...
                'Callback','uisetcolor(gco,''Line Color'');pt(''DeletePointMarker'');');
            
            % Line width option
            uimenu(contextMenu,'Label','Line Width++',...
                'Callback','pt(''LineWidth'');');

            % Export menu
            contextMenuExport = uimenu(contextMenu,'Label','Export Channel',...
                'Separator','On');
            uimenu(contextMenuExport,'Label','To base workspace',...
                'Tag','LineMenuExport',...
                'Callback','pt(''export'');');
            uimenu(contextMenuExport,'Label','To Channel Manager',...
                'Tag','LineMenuExportChanman',...
                'Callback','pt(''export'');');
            uimenu(contextMenuExport,'Label','To Variable Editor',...
                'Tag','LineMenuExportVariableEditor',...
                'Callback','pt(''export'');');

            
            % Delete line option
            uimenu(contextMenu,'Label','Delete',...
                'Separator','On',...
                'Callback','pt(''DeletePointMarker'');delete(gco);');
            
            % Associate with all lines on plot
            set(findobj(get(axesHandle(i),'Children'),'Type','line'),...
                'UIContextMenu',contextMenu);
            
        end
        
    case 'stringedit'
        
        % Get object handle
        handle = get(axesHandle,varargin{1});
        string = get(handle,'String');
        
        % Create dialog box
        prompt  = {'String:'};
        default = {string}   ;
        titlestr   = 'Modify Text';
        lineNo  = 1;
        string  = inputdlg(prompt,titlestr,lineNo,default,'off');
        
        % Set the string
        if ~isempty(string),
            set(handle,'string',string);
        end
        
    case 'labeltoggle'
        
        % Get handle
        handle = findobj('Tag','LabelMenu');
        state  = get(handle,'Checked');
        
        if strcmp(state,'off'),
            % Check it and enable submenu
            set(handle,'Checked','On');
            set(findobj('Tag','LabelTextMenu'),'Enable','On');
        else
            % Uncheck it and disable submenu
            set(handle,'Checked','Off');
            set(findobj('Tag','LabelTextMenu'),'Enable','Off');
        end
        
    case 'linecontextmenu'
        
        % Get the axes currentpoint
        lineHandle = gco;
        xData      = get(lineHandle,'XData');
        yData      = get(lineHandle,'YData');
        axesPoint  = get(axesHandle,'CurrentPoint');
        xPoint     = axesPoint(1,1);
        yPoint     = axesPoint(1,2);
        
        % Find closest point
        % Errors in each axis
        xError = xData - xPoint;
        yError = yData - yPoint;
        
        % Scale errors to same size
        xError = xError/max(abs(xError));
        yError = yError/max(abs(yError));
        error  = [xError; yError];
        error  = error.^2;
        error  = sum(error);
        index  = find(min(error)==error);
        
        %    data     = [xData;yData];
        %    distance = data - [xPoint;yPoint]*ones(1,size(data,2)); % Calc dist to each data point
        %    distance = distance.^2;     % Square of distances.
        %    distance = sum(distance); % Sum the squares.
        %    index    = find(min(distance)==distance);
        
        % Set the menu labels and callbacks
        name = get(lineHandle,'DisplayName');
        if isempty(name),
            name = 'None';
        end
        meta = getappdata(lineHandle,'ChannelMetadata');
        set(findobj('Tag','LineMenuName'),...
            'Label',['Name: ' name],...
            'Callback','pt(''LineMetadata'');',...
            'Userdata',meta);
        
        % Metadata submenu
        if ~isempty(meta),
            
        end
        
        xString = ['evalin(''base'',''ans = ' num2str(xData(index)) ''')'];
        yString = ['evalin(''base'',''ans = ' num2str(yData(index)) ''')'];
        set(findobj('Tag','LineMenuX'),...
            'Label',['X: ' num2str(xData(index))],...
            'Callback',xString);
        set(findobj('Tag','LineMenuY'),...
            'Label',['Y: ' num2str(yData(index))],...
            'Callback',yString);
        set(findobj('Tag','LineMenuInd'),...
            'Label',['Ind: ' num2str(index)]);
        set(findobj('Tag','LineMenuMarkX'),...
            'Userdata',[xData(index) yData(index)],...
            'Callback','pt(''MarkX'');');
        set(findobj('Tag','LineMenuMarkY'),...
            'Userdata',[xData(index) yData(index)],...
            'Callback','pt(''MarkY'');');
        set(findobj('Tag','LineMenuMarkXY'),...
            'Userdata',[xData(index) yData(index)],...
            'Callback','pt(''MarkXY'');');
        set(findobj('Tag','LineMenuMarkText'),...
            'Userdata',[xData(index) yData(index)],...
            'Callback','pt(''MarkText'');');
        
        % Set the stat menu data
        set(findobj('Tag','StatMenuSamples'),...
            'Label',['Samples: ' num2str(length(yData))]);
        set(findobj('Tag','StatMenuMean'),...
            'Label',['Mean: ' num2str(mean(yData))]);
        set(findobj('Tag','StatMenuRMS'),...
            'Label',['RMS: ' num2str(sqrt(mean(yData.^2)))]);
        set(findobj('Tag','StatMenuDev'),...
            'Label',['Standard Deviation: ' num2str(std(yData))]);
        set(findobj('Tag','StatMenuMax'),...
            'Label',['Maximum: ' num2str(max(yData))]);
        set(findobj('Tag','StatMenuMin'),...
            'Label',['Minimum: ' num2str(min(yData))]);
        
        % Mark the point
        nextPlot = get(gca,'NextPlot');
        set(gca,'NextPlot','Add');
        pointHandle = plot(xData(index),yData(index),'r+');
        %   set(pointHandle,'Color',not(get(lineHandle,'Color'))&[1 1 0]);
        set(pointHandle,'ButtonDownFcn','delete(gco);',...
            'Tag','PointMarker');
        set(gca,'NextPlot',nextPlot);
                
        % Highlight the line
        %   set(lineHandle,'LineWidth',2);
        
    case 'markx'
        pnt = get(findobj('Tag','LineMenuMarkX'),'Userdata');
        if iscell(pnt), pnt = pnt{1}; end
        str = num2str(pnt(1));
        pnt = pnt + 0.02*[diff(get(gca,'xlim')) diff(get(gca,'ylim'))];
        textHandle = text(pnt(1),pnt(2),str);
        set(textHandle,'ButtonDownFcn','delete(gco);',...
            'BackgroundColor','w',...
            'EdgeColor','k');
        
    case 'marky'
        pnt = get(findobj('Tag','LineMenuMarkX'),'Userdata');
        if iscell(pnt), pnt = pnt{1}; end
        str = num2str(pnt(2));
        pnt = pnt + 0.02*[diff(get(gca,'xlim')) diff(get(gca,'ylim'))];
        textHandle = text(pnt(1),pnt(2),['(' str ')']);
        set(textHandle,'ButtonDownFcn','delete(gco);',...
            'BackgroundColor','w',...
            'EdgeColor','k');
        
    case 'markxy'
        pnt = get(findobj('Tag','LineMenuMarkX'),'Userdata');
        if iscell(pnt), pnt = pnt{1}; end
        str = ['(' num2str(pnt(1)) ',' num2str(pnt(2)) ')'];
        pnt = pnt + 0.02*[diff(get(gca,'xlim')) diff(get(gca,'ylim'))];
        textHandle = text(pnt(1),pnt(2),str);
        set(textHandle,'ButtonDownFcn','delete(gco);',...
            'BackgroundColor','w',...
            'EdgeColor','k');
        
    case 'labelxtoggle'
        % Set callback function
        get(gcbf,'WindowButtonUpFcn','LabelX');
        
    case 'stattoggle'
        % Toggle the state of the marked or to workspace menu items
        han = gcbo;
        if strcmp(get(han,'checked'),'on'),
            set(han,'checked','off');
        else
            set(han,'checked','on');
        end
        
    case 'statcallback'
        % Get raw data
        lineHandle = gco;
        yData      = get(lineHandle,'YData');
        
        % Define value and strings depending on which statistic is wanted
        switch get(gcbo,'Tag')
            case 'StatMenuSamples'
                value = length(yData);
                string = 'N = ';
            case 'StatMenuMean'
                value = mean(yData);
                string = 'Mean = ';
            case 'StatMenuRMS'
                value = sqrt(mean(yData.^2));
                string = 'RMS = ';
            case 'StatMenuDev'
                value = std(yData);
                string = '\sigma_x = ';
            case 'StatMenuMax'
                value = max(yData);
                string = 'Max = ';
            case 'StatMenuMin'
                value = min(yData);
                string = 'Min = ';
        end
        
        % Find the callback objects parent, and the parents children
        % so that we know we have the right menu.
        parent = get(gcbo,'parent');
        children = get(parent,'Children');
        
        if strcmp(get(findobj(children,'Tag','StatMenuMark'),'Checked'),'on')
            % Mark on the plot
            xPos = sum(get(gca,'xlim'))/2;
            yPos = sum(get(gca,'ylim'))/2;
            textHandle = text(xPos,yPos,[string num2str(value)]);
            set(textHandle,'backgroundcolor','w',...
                'edgecolor','k');
            % Add the context menu
            maketextmenu(textHandle);
            
        end
        
        if strcmp(get(findobj(children,'Tag','StatMenuWorkspace'),'Checked'),'on')
            % Dump to workspace
            evalin('base',['ans = ' num2str(value) ]);
        end
        
        pt('DeletePointMarker');

        
    case 'textmove'
        
        % Toggle state of text dragability
        textHandle = gco;
        
        % Get the new point
        disp('Select new text location.')
        [x,y] = ginput(1);
        
        % Move the text
        set(textHandle,'Position',[x y]);
        
    case 'holdtoggle'
        
        % Get menu item checked status
        han = gcbo;
        checkStatus = get(han,'Checked');
        
        % Swap the hold state to the opposite of the checkStatus
        if strcmpi(checkStatus,'on'),
            set(gca,'NextPlot','Replace');
        else
            set(gca,'NextPlot','Add');
        end
        
    case 'ptaxescontextmenuselected'
        
        % Check the axes hold state
        axHan = gca;
        holdState = get(axHan,'NextPlot');
        
        % Set hold menu item check
        menuItemHan = findobj(get(gcf,'children'),'Tag','ToggleHoldMenu');
        if strcmpi(holdState,'replace'),
            set(menuItemHan,'Checked','Off');
        else
            set(menuItemHan,'Checked','On');
        end
        
    case 'linemetadata'
        % Displays any metadata associated with the channel object.
        meta = get(gcbo,'userdata');
        disp(meta);
        
    case 'linewidth'
        han = gco;
        width = get(han,'LineWidth');
        
        % Line width increases.
        target_widths = [0.5 2 3 4 5 5];
        idx = find(width == target_widths,1,'first');
        idx = idx + 1;
        
        width = target_widths(idx);
        set(han,'LineWidth',width);
        
        % Delete the point marker
        pt('DeletePointMarker');

    case 'export'
        % Export data as a channel
        
        % Get data
        han = gco;
        xdata = get(han,'XData');
        ydata = get(han,'YData');
        
        % Get channel name
        name = get(han,'DisplayName');
        if isempty(name)
            name = 'channel';
        end
        
        % Let user validate
        name = inputdlg('Channel Name:',...
            'Channel Export',...
            1,...
            {name});
        name = name{1};
        
        % Delete the point marker
        pt('DeletePointMarker');
        
        % Force to a valid name.
        name = genvarname(name);
        
        % Try to get units
        axhan = get(han,'Parent');
        yl = get(get(axhan,'YLabel'),'String');
        
        % Look for '[]'
        unit_str = regexp(yl,'\[.*\]','match');
        
        % Look for '()
        if isempty(unit_str)
            unit_str = regexp(yl,'\(.*\)','match');
        end
        
        % Clean up
        if ~isempty(unit_str)
            unit_str = unit_str{1};
            if length(unit_str) > 2
                unit_str = unit_str(2:end-1);
            else
                unit_str = '';
            end
        end
        
        % Form channel
        if isempty(unit_str)
            ch = channel(name,ydata,xdata);
        else
            ch = channel(name,ydata,xdata,unit_str);
        end            
        
        % Export target
        switch(get(gcbo,'Tag'))
            case 'LineMenuExport'
                assignin('base',name,ch);
            case 'LineMenuExportChanman' 
                chanman;
                chanman('AddChannel',ch);
                
            case 'LineMenuExportVariableEditor'
                table(ch);
            otherwise
                assert(0);
        end
        
        % UI 
        display(['Data exported to workspace as: "' name '"']);
        
    case 'deletepointmarker'
        % Delete the last point marker
        axhan = get(gco,'Parent');
        pthan = findobj(get(axhan,'Children'),'Tag','PointMarker');
        if isempty(pthan)
            return;
        end
        
        % Delete it
        pthan = pthan(1);
        delete(pthan);
        
    case 'histogram'

        % Get data
        han = gco;
        ydata = get(han,'YData');

        % Clean up before creating any new UI objects.
        pt('DeletePointMarker');
        
        % Calc number of bins.
        n_pts = length(ydata);
        if n_pts < 100
            n_bins = 10;
        elseif n_pts < 500
            n_bins = 30;
        else
            n_bins = 50;
        end
        
        % Create histogram
        figure;
        hist(ydata,n_bins);
        
        % Plot niceties
        axhan = get(han,'Parent');
        yl = get(get(axhan,'YLabel'),'String');
        xlabel(yl);
        ylabel('Count');

        name = get(han,'DisplayName');
        if ~isempty(name)
            title(['Histgram of ' name]);
        end

        % Histogram legend.
        histlegend(ydata);
        
    case 'metatitle'
        % Put file name for axes title.
        meta = getappdata(gco,'ChannelMetadata');
        title(meta.sourcefile,'Interpreter','None');
        
        % Cleanup
        pt('DeletePointMarker');
end

end % main

%---------------------------------------------------------
% Local functions
%---------------------------------------------------------

function [] = maketextmenu(textHandle)

% Create the text context menu
contextMenu = uicontextmenu('Tag','TextContextMenu');
uimenu(contextMenu,'Label','Move',...
    'Callback','pt(''TextMove'');',...
    'Tag','TextMenuDraggable');
uimenu(contextMenu,'Label','Delete',...
    'Separator','On',...
    'Callback','delete(gcbo);delete(gco);');

% Attach to text
set(textHandle,'UIContextMenu',contextMenu);
end  % maketextmenu

function [] = AxesEdit(varargin)
propedit(gca,'-noselect');
end % AxesEdit


function [] = TimeWindowAvg(varargin)
% Generate a time windowed avg plot.
kids = findobj(get(gca,'Children'),'Type','line');
if isempty(kids)
    return;
end

lineHan = kids(1);
xData = get(lineHan,'XData');
dt  = unique(diff(xData));
dt(diff(dt) < 1e-6) = [];
if length(dt) > 1
    error('Data must be evenly sampled.');
end

yData = get(kids,'ydata');
name = get(lineHan,'DisplayName');
ch = channel(name,yData,dt);
figure;
mtimeavg(ch,20);

end