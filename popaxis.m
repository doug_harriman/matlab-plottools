%POPAXIS  Set axis limits to saved values.
%

% DLH
% 09/26/01 - Created

function []=popaxis(axes)

% Default to current figure
figHan = gcf;

% Default to xy
if nargin < 1
    axes = 'xy';
end

% Get axis limits from PUSHAXIS
axisLimits = pushaxis('Retrieve');

if isempty(axisLimits),
    return;
end

% Store out axis limits
axesHans = gca; %findobj(get(figHan,'Children'),'Type','Axes');
for i = 1:length(axesHans)
    switch(axes)
        case 'xy'
            set(axesHans(i),'xlim',axisLimits{i}.x);
            set(axesHans(i),'ylim',axisLimits{i}.y);
            set(axesHans(i),'zlim',axisLimits{i}.z);
        case 'x'
            set(axesHans(i),'xlim',axisLimits{i}.x);
        case 'y'
            set(axesHans(i),'ylim',axisLimits{i}.y);
        otherwise
            error(['Unsupported axes: ' axes]);
    end
end
