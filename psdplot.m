%PSDPLOT  Creates a PSD of the contents of an axes set.
%  PSD_AXES_HANDLE = PSDPLOT(AXES_HANDLE,PSD_AXES_HANDLE) 
%  creates a power spectral density plot of the contents 
%  currently visible in AXES_HANDLE in the axes PSD_AXES_HANDLE.  
%  The data will be modified to have zero mean and then 
%  Hanning windowed before the Fourier transform is applied.
%
%  If AXES_HANDLE is not specified, the current axes will be used.
%
%  If PSD_AXES_HANDLE is not specified a new axes set will
%  be created in the current figure if the current figure contains
%  only one axes set.  Otherwise a new figure will be created.
%
%  Multiple calls from the same AXES_HANDLE will continually update
%  the newly created PSD_AXES_HANDLE.
%
%  The PSD plot will only be created for the first data set in 
%  AXES_HANDLE.  In addition, the data set must have evenly 
%  spaced X-axis data.
%
%  Written by: Doug Harriman (doug_harriman@comcast.net)
%

% DLH
% 05/18/04 - Created

function [psdAxHan] = psdplot(dataAxHan,psdAxHan)

% Error check inputs
if nargin > 2, error('Too many input arguments.'); end
if nargin == 0, dataAxHan = gca; end
if ~isa(dataAxHan,'matlab.graphics.axis.Axes'), error('Axes handle required.'); end
if length(dataAxHan) > 1, error('AXES_HANDLE must be 1x1.'); end
if nargin == 2,
    if ~isa(psdAxHan,'double'), error('PSD_AXES_HANDLE must be of type double.'); end
    if length(psdAxHan) > 1, error('PSD_AXES_HANDLE must be 1x1.'); end
end

% Validate data to be FFT'd
% Find handle of data line
lineHan = findobj(get(dataAxHan,'Children'),'Type','line','HandleVisibility','On');
if isempty(lineHan), error('No data in current axes set.'); end
lineHan = lineHan(1);

% Make sure it's evenly sampled
xData = get(lineHan,'XData');
dt  = unique(diff(xData));
dt(diff(dt) < 1e-6) = [];
if length(dt) > 1, error('Data must be evenly sampled.'); end

% Extract the portion that's currently visible.
xLim = get(dataAxHan,'xLim');
ind = (xData >= xLim(1)) & (xData <= xLim(2)); 

% Clean up the data
yData = get(lineHan,'YData');
yData = yData(ind);
yData = ZeroMean(yData);
yData = Hanning(yData);

% Calc the FFT & PSD
yFFT = fft(yData) ;  % do FFT
maxW = (1/dt);
dw   = maxW/length(yFFT);

% Only plot lower 1/2 of data
ind  = round(length(yFFT)/2) ;  % First half of the data set
yFFT = yFFT(1:ind);
w    = [0:dw:dw*(length(yFFT)-1)];
p    = abs(yFFT) ; % 2 Norm of coeffs
p    = p/length(yFFT); % Normalize


% See if we already have a defined psdAxHan
psdAxHan = getappdata(lineHan,'PSDAxHan');
if ishandle(psdAxHan),
    axes(psdAxHan);
    
    % Update plot, but don't change axes limits user may have set.
    lineHan = get(psdAxHan,'Children');
    lineHan = lineHan(1);
    set(lineHan,'XData',w,'YData',p);
    
elseif nargin < 2
    % PSD axes not specified.
    fig  = get(dataAxHan,'Parent');
    kids = findobj(get(fig,'Children'),'Type','axes');
    
    if length(kids) == 1,
        % Only one axes in figure, so add this one.
        s1 = [0.1300    0.5811    0.7750    0.3439];
        s2 = [0.1300    0.1100    0.7750    0.3439];
        set(dataAxHan,'Units','Normalized',...
            'Position',s1);
        figure(fig);
        psdAxHan = axes('Position',s2);

        % Do the plot
        plot(w,p);
        xlabel('Frequency');
        ylabel('PSD');
        
    else
        % Figure already cluttered, so create new figure.
        figure;
        psdAxHan = axes;
    end
else
    % PSD axes specified, so plot there.
    axes(psdAxHan);
end

% Store out the PSD plot handle so doesn't have to be respecified
setappdata(lineHan,'PSDAxHan',psdAxHan);

% Enable plot tools if exist.
if exist('pt') %#ok<EXIST>
    pt;
end

% No output if not requested
if nargout == 0,
    clear psdAxHan
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Helper Functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%---------------------------------------------------------------------
% ZeroMean
% Set the mean of a vector to zero.
%---------------------------------------------------------------------
function [out] = ZeroMean(in)
out = in - mean(in);

%---------------------------------------------------------------------
% Hanning
% Applies a Hanning window to a data vector
%---------------------------------------------------------------------
function [out] = Hanning(in)

% Create the window
pnts = length(in);
delta = 2*pi/pnts;
time = [delta:delta:2*pi];
win = -0.5*cos(time)+0.5;
if size(win) ~= size(in),
    win = win';
end

% Apply it
out = in.*win;
