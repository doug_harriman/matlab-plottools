%STDBOX  Box plot showing 3 sigma data.
%

% DLH
% 07/14/04 - Created

function [] = stdbox(data,labels)

% Error check inputs
if nargin > 2, error('Too many inputs.'); end
if nargin < 1, error('Not enough inputs.'); end
if ~isnumeric(data), error('input 1 must be numeric.');end
if nargin >1 & min(size(labels)) > 1, error('input 2 must be Nx1 or 1xN');end
if nargin == 2,
    if size(data,1) ~= length(labels),
        error('length of LABELS must match number of rows of DATA.');
    end
end

% Compute stats for each column.
meanData = mean(data);
stdData  = std(data);
maxData  = max(data);
minData  = min(data);

% Generate the plot
h = plot(maxData,'.');
hold on;
h = [h plot(minData,'.')];
h = [h plot(meanData+3*stdData,'o')];
h = [h plot(meanData-3*stdData,'o')];
h = [h plot(meanData,'+')];

for i=1:length(meanData),
    h = [h plot([1 1]*i,[minData(i) maxData(i)])];
end

