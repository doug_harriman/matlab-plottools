%ANIMTEST2  ANIMOBJ example/test script.
%  Demonstrates a custom frame capture function: animcapture.m
%
%  See also: animobj, animcapture.
%

% Doug Harriman (doug.harriman@comcast.net)

% Create a quick plot.
x=[0:20];
y=rand(length(x),1);
set(gca,'xlim',[min(x) max(x)]);
set(gca,'ylim',[0 1]);
h = plot(x,y,'r');

% New five frame animation
animobj('NewAnimation');
animobj('SetTotalFrames',5);

% Change the line width and color at each frame.
animobj('AddObject',h);
animobj('SetAnimateProperty',h,'Color',[1 0 0],[0 0 1]);
animobj('SetAnimateProperty',h,'LineWidth',1,5);

% Use animcapture.m to capture frames as an example.
animobj('SetFrameCaptureFcn','animcapture');
animobj('Animate');
