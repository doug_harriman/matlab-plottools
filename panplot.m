%PANPLOT Pan around a plot by dragging the cursor.
%
%   Written by: Doug Harriman (doug_harriman@comcast.net)
%

% DLH
% 08/14/02 - Created

% Improvements
% Activate pan via a hotkey

function [] = panplot(msg)

if nargin == 0,
    msg = 'on';
end

persistent oldPointer
persistent oldPoint
persistent oldRenderer

switch(lower(msg)),
case 'on'
    set(gca,'ButtonDownFcn','panplot(''ButtonDown'');');

    % Pointer
    oldPointer = get(gcf,'Pointer');
    set(gcf,'Pointer','Cross');

    % Renderer for smooth motion
    oldRenderer = get(gcf,'Renderer');
    set(gcf,'Renderer','OpenGL');
    
    % Clean up from previous runs
    oldPoint = [];
    
case 'buttondown'
    set(gcf,'WindowButtonUpFcn','panplot(''Off'');');
    set(gcf,'WindowButtonMotionFcn','panplot(''Panning'');');
    
case 'panning'
    pt = get(gca,'CurrentPoint');
    
    newPoint = pt(1,1:2);
    if isempty(oldPoint),
        oldPoint = newPoint;
        return;
    end
    
    % Calc motion
    delta = -(oldPoint - newPoint);
    
    % Do the pan
    xLim = get(gca,'xlim');
    xRange = diff(xLim);
    xMove  = delta(1);
    
    yLim = get(gca,'ylim');
    yRange = diff(yLim);
    yMove  = delta(2);
    
    xLim(1) = xLim(1) - xMove;
    xLim(2) = xLim(1) + xRange;
    
    yLim(1) = yLim(1) - yMove;
    yLim(2) = yLim(1) + yRange;

    set(gca,'xlim',xLim);
    set(gca,'ylim',yLim);

    % Update the old point (include how much we moved the axes)
    oldPoint = newPoint - delta;
    
case 'off'
    set(gca,'ButtonDownFcn','');
    set(gcf,'WindowButtonUpFcn','');
    set(gcf,'WindowButtonMotionFcn','');
    set(gcf,'Pointer',oldPointer);
    set(gcf,'Renderer',oldRenderer);
    
end