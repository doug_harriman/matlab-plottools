%POINTERWINDOW  Returns handle of figure that pointer currently is over.
%  HANDLE=POINTERWINDOW returns the HANDLE of the Matlab figure which the
%  pointer is currenlty over.  Returns [] if not over any figures.
%

function han = pointerwindow

% Get cursor location and current figure positions.
ptr = get(0,'PointerLocation');

fig_han = get(0,'Children');
fig_pos = get(fig_han,'Position');

tmp = @(y) test(ptr,y);
tf = cellfun(tmp,fig_pos);   

han = fig_han(tf);

end

function tf = test(ptr,pos)

tf = true;
if ptr(1) < pos(1)
   tf = false;
   return;
end

if ptr(2) < pos(2)
   tf = false;
   return;
end

if ptr(1) > pos(1) + pos(3)
   tf = false;
   return;
end

if ptr(2) > pos(2) + pos(4)
   tf = false;
   return;
end


end
