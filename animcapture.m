%ANIMCAPTURE  Example function to capture frames to generate movie.
% 
% If doing a Matlab movie, calling with no inputs will return movie object.
%
% See also: animobj, animtest2.
% 

% Doug Harriman (doug.harriman@comcast.net)

function [varargout] = animcapture(this_frame,total_frames)

persistent M

% Support two animation types.
anim_type = 'Matlab Movie';  
% anim_type = 'PNG Frames';

% If called with no arguments, return M
if nargin == 0,
    varargout{1} = M;
    return;
end

% Capture the frame.
switch(anim_type),
    case 'Matlab Movie',
        if this_frame == 1,
            M(1).cdata=[];
            M(1).colormap=[];
            M=M(1);
        end
        
        pause(.5);
        M(this_frame) = getframe(gcf);
        
    case 'PNG Frames', 

        % Generate PNG image
        file_name = ['frame_' num2str(this_frame) '.png'];
        print('-dpng','-r300',file_name);

    otherwise,
        error(['Unsupported animation type: ' anim_type]);
end