%FLIPXY  Flips plot X and Y axes.
%

% DLH
% 05/24/05 - Created

function [] = flipxy(ax)

% Error check inputs
if nargin > 1, error('One input expected.'); end
if nargin == 0, ax = gca; end

% List of axes properties to flip
ax_prop_list = {'Dir','Grid','Label','Lim','LimMode','Scale'};%,...
%              'Tick','TickLabel','TickLabelMode','TickMode'};

% List of line properties to flip.
line_prop_list = {'Data','DataSource'};
             
% Loop through all axes, lines and properties of each.
for i = 1:length(ax),
    % Flip axes
    for j = 1:length(ax_prop_list),
        swap_prop(ax(i),ax_prop_list{j});
    end
    
    % Flip lines
    child_list = get(ax(i),'Children');
    for j = 1:length(child_list),
        for k = 1:length(line_prop_list),
            swap_prop(child_list(j),line_prop_list{k});
        end
    end
end


% Swaps a given axes property between the X and Y axes.
function [] = swap_prop(handle,prop)
    temp = get(handle,['x' prop]);
    
    if ishandle(temp),
        set(handle,['z' prop],get(handle,['x' prop]));
        set(handle,['x' prop],get(handle,['y' prop]));
        set(handle,['y' prop],get(handle,['z' prop]));
    else,
        set(handle,['x' prop],get(handle,['y' prop]));
        set(handle,['y' prop],temp);
    end
    
