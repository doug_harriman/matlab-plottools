%DZOOM  Dynamic zoom for a plot.
%   DZOOM creates a box on the current plot.  The contents of this box are
%   shown in a new figure.  The box may be dragged around the plot, and may
%   be resized via the handles on the box.
%
%   DZOOM is capable of controlling the axis limits of other axes objects.
%   Right click on the box and select 'Attach Axes'.  Then select the axis
%   which axis you wish to control: X, Y or both X and Y.  Finally, select
%   the axes set to which this zoom control should be applied.
%
%   Written by: Doug Harriman (doug.harriman@gmail.com)
%

% 02-Oct-08 - Slightly better handling of multi-axis figures.
% 05/18/04 - Added live PSD capability through PSD plot.
% 05/11/04 - Converted to resize on corners.
% 04/23/04 - Created.  Originally draggable graphics based on 'data_marker.m' by Scott Hirsch.

% Bugs:
% - Manage axes buttondown fcn when attaching so don't overwrite.

% Future Improvements:
% - Add manual detach capability
% - Manage axes delete fcn to force a detach.
% - Dialog to select axes to attach.
% - Could probably speed things up if moved from array of .handle, .limits
% to three arrays: arrayXY arrayX and arrayY.  Then could just set list of
% handles instead of looping through lists.
% - Would be nice to have the box labelled when second zoom box is added.

function [] = dzoom

% Get data out of current figure before we start adding stuff
origFig  = gcf;
childHan = get(gcf,'Children');
origAx   = gca;

% Determine new figure window placement
set(0,'Units','Pixels');
rootSize = get(0,'ScreenSize');
rootUnits = get(0,'Units');
origFigPos = get(origFig,'Position');
yMargin = rootSize(4) * .07;
xMargin = rootSize(3) * .005;
zoomFigPos = origFigPos;
if (rootSize(3) - origFigPos(1) - 2*origFigPos(3)  - xMargin) > 0, % Right
    zoomFigPos([2,4]) = origFigPos([2,4]);
    zoomFigPos(1) = origFigPos(1) + origFigPos(3) + xMargin;
elseif (origFigPos(2) - origFigPos(4) - yMargin) > 0,  % Below
    zoomFigPos([1,3]) = origFigPos([1,3]);
    zoomFigPos(2) = origFigPos(2) - zoomFigPos(4) - yMargin;
elseif (rootSize(4) - origFigPos(2) - origFigPos(4) - origFigPos(4) - yMargin) > 0, % Above
    zoomFigPos([1,3]) = origFigPos([1,3]);
    zoomFigPos(2) = origFigPos(2) + origFigPos(4) + yMargin;
elseif (origFigPos(1) - origFigPos(3) - xMargin) > 0, % Left
    zoomFigPos([2,4]) = origFigPos([2,4]);
    zoomFigPos(1) = origFigPos(1) - origFigPos(3) - xMargin;
end
zoomFig = figure('Position',zoomFigPos,...
    'Units',rootUnits);%,...
%     'HandleVisibility','callback');

% Create the copy
copyobj(childHan,zoomFig);

% Figure out which axis we're zooming in the new figure.
idx = childHan == origAx;
newKids = get(zoomFig,'Children');
zoomAx  = newKids(idx);

% If this was the second zoom box placed, then we copied the box lines and
% handles from the first zoombox.  Delete those now.
kids   = get(zoomAx,'Children');
oldHan = findobj(kids,'Tag','ZoomBoxElement');
oldHan = [oldHan; findobj(kids,'Tag','UR')];
oldHan = [oldHan; findobj(kids,'Tag','UL')];
oldHan = [oldHan; findobj(kids,'Tag','LR')];
oldHan = [oldHan; findobj(kids,'Tag','LL')];
set(oldHan,'DeleteFcn','');
delete(oldHan);


% Switch back to original figure
figure(origFig);

data.origFig = origFig;
data.origAx  = origAx;

% Hold status
hold('on');

% Create a box in the center of the axis
xLim = get(gca,'xlim');
yLim = get(gca,'ylim');
xCenter = mean(xLim);
yCenter = mean(yLim);
xRange  = Range(xLim);
yRange  = Range(yLim);
boxXLen = 0.33 * xRange;
boxYLen = 0.33 * yRange;
boxXRange = [-1 1]*0.5*boxXLen + xCenter;
boxYRange = [-1 1]*0.5*boxYLen + yCenter;

% Draw the box
boxLeftHan   = plot([1 1]*min(boxXRange),boxYRange,'k');
boxRightHan  = plot([1 1]*max(boxXRange),boxYRange,'k');
boxTopHan    = plot(boxXRange,[1 1]*max(boxYRange),'k');
boxBottomHan = plot(boxXRange,[1 1]*min(boxYRange),'k');
boxHan = [boxLeftHan boxRightHan boxTopHan boxBottomHan];
set(boxHan,...
    'Tag','ZoomBoxElement',...
    'ButtonDownFcn',@DownFcn,...
    'DeleteFcn',@BoxDelete,...
    'EraseMode','xor',...
    'LineWidth',1);
%     'HandleVisibility','Callback',...

% Store out the data we'll need later
data.dragStartPoint = [];
data.boxHandles = boxHan;

% Draw the handles
dragLLHan = plot(min(boxXRange),min(boxYRange),'sk');
dragLRHan = plot(max(boxXRange),min(boxYRange),'sk');
dragULHan = plot(min(boxXRange),max(boxYRange),'sk');
dragURHan = plot(max(boxXRange),max(boxYRange),'sk');
dragHan = [dragLLHan dragLRHan dragULHan dragURHan];
data.dragHandles = dragHan;
set(dragHan,...
    'MarkerSize',5,...
    'MarkerFaceColor','k',...
    'ButtonDownFcn',@DownFcn,...
    'DeleteFcn',@BoxDelete,...
    'HandleVisibility','Callback',...
    'EraseMode','xor');
set(dragLLHan,'Tag','LL');
set(dragLRHan,'Tag','LR');
set(dragULHan,'Tag','UL');
set(dragURHan,'Tag','UR');

% Other data not initially used
data.attachedAxes = [];
data.zoomFig      = zoomFig;

% Store out all of the data
set(boxHan,'UserData',data);
set(dragHan,'UserData',data);

% Set up figure for fast plotting
set(gcf,'DoubleBuffer','on')
if strcmp(get(gcf,'renderer'),'painters'),
    set(gcf,'renderer','zbuffer');
end

% Zoom Figure Name
origFigName = get(origFig,'Name');
if isempty(origFigName),
    origFigName = ['Figure No. ' num2str(origFig)];
end
set(zoomFig,...
    'Visible','On',...
    'NumberTitle','Off',...
    'UserData',data,...
    'Name',['Zoom: ' origFigName]);

% Context menu
figure(origFig);
cMenu = uicontextmenu;
set([boxHan dragHan],'UIContextMenu',cMenu);
attachMenu = uimenu(cMenu,'Label','Attach Axes');
uimenu(attachMenu,'Label','Control X','Tag','x','Callback',@AttachAxes);
uimenu(attachMenu,'Label','Control Y','Tag','y','Callback',@AttachAxes);
uimenu(attachMenu,'Label','Control XY','Tag','xy','Callback',@AttachAxes);
%uimenu(cMenu,'Label','Detach Axes');

uimenu(cMenu,...
    'Label','Delete',...
    'Separator','On',...
    'Callback','delete(gco);');

% Add the new axes to the list of controlled axes
data.attachedAxesLimits = 'xy';
axes(zoomAx);
pt;
set(zoomAx,'UserData',data);
RegisterAxes;


%-------------------------------------------------------------------
% DownFcn: Handles mouse button down event
%-------------------------------------------------------------------
function DownFcn(varargin)
% Callbcak for Left mouse click on object
set(gcf,'WindowButtonMotionFcn',@MoveFcn)
set(gcf,'WindowButtonUpFcn',@UpFcn)
pt = get(gca,'CurrentPoint');
pt = pt(1,1:2);
data = get(gco,'UserData');
data.dragStartPoint = pt;
set(gco,'UserData',data);


%-------------------------------------------------------------------
% UpFcn: Handles mouse button up event
%-------------------------------------------------------------------
function UpFcn(varargin)
% Callback for mouse button release
set(gcf,'WindowButtonMotionFcn',[])


%-------------------------------------------------------------------
% MoveFcn: Handles mouse drag
%-------------------------------------------------------------------
function MoveFcn(varargin)

% Do the drag
han = gco;

% Bail if not on a line.
if ~strcmp(get(han,'Type'),'line'),
    return;
end

% Current Point
axHan = gca;
point = get(axHan,'CurrentPoint');
curPt = point(1,1:2);

% Our data
data = get(han,'UserData');

% Shift
dPt = curPt - data.dragStartPoint;

% Get length of data vector
xData = get(han,'xdata');
if length(xData) > 1,
    % Picked a line, so move the whole box
    h = [data.boxHandles data.dragHandles];

    for i = 1:length(h),
        x = get(h(i),'XData');
        y = get(h(i),'YData');

        x = x + dPt(1);
        y = y + dPt(2);
        set(h(i),...
            'Xdata',x,...
            'Ydata',y,...
            'UserData',data);
    end

else
    % Point data
    obj = gco;
    tag = get(obj,'tag');
    x = get(obj,'XData');
    y = get(obj,'YData');

    % Axes data
    xLim = get(data.origAx,'xLim');
    yLim = get(data.origAx,'yLim');

    % Min zoom size
    minZoomSize = 0.05; % 5% of window.
    xZoomLimits = get(data.boxHandles(3),'XData');
    yZoomLimits = get(data.boxHandles(1),'YData');
    minX = xZoomLimits(1) + Range(xLim)*minZoomSize;
    maxX = xZoomLimits(2) - Range(xLim)*minZoomSize;
    minY = yZoomLimits(1) + Range(yLim)*minZoomSize;
    maxY = yZoomLimits(2) - Range(yLim)*minZoomSize;

    % Enforce limits
    switch(tag)
        case 'LL',
            minX = xLim(1);
            minY = yLim(1);
        case 'LR',
            maxX = xLim(2);
            minY = yLim(1);
        case 'UL',
            minX = xLim(1);
            maxY = yLim(2);
        case 'UR',
            maxX = xLim(2);
            maxY = yLim(2);
    end

    if (x>maxX), x=maxX; end
    if (x<minX), x=minX; end
    if (y>maxY), y=maxY; end
    if (y<minY), y=minY; end

    % Calculate the new point
    x = x + dPt(1);
    y = y + dPt(2);

    % Move the other objects
    switch(tag),
        case 'LL',
            % Move the other two points
            set(data.dragHandles(2),'YData',y);
            set(data.dragHandles(3),'XData',x);

            % Move the lines
            yData = get(data.boxHandles(1),'yData');
            set(data.boxHandles(1),'xData',[1 1]*x);
            set(data.boxHandles([1,2]),'yData',[y yData(2)]);
            xData = get(data.boxHandles(4),'xData');
            set(data.boxHandles(4),'yData',[1 1]*y);
            set(data.boxHandles([3,4]),'xData',[x xData(2)]);

        case 'LR',
            % Move the other two points
            set(data.dragHandles(1),'YData',y);
            set(data.dragHandles(4),'XData',x);

            % Move the lines
            yData = get(data.boxHandles(1),'yData');
            set(data.boxHandles(2),'xData',[1 1]*x);
            set(data.boxHandles([1,2]),'yData',[y yData(2)]);
            xData = get(data.boxHandles(4),'xData');
            set(data.boxHandles(4),'yData',[1 1]*y);
            set(data.boxHandles([3,4]),'xData',[xData(1) x]);

        case 'UL'

            % Calculate the new point
            x = x + dPt(1);
            y = y + dPt(2);

            % Move the other two points
            set(data.dragHandles(4),'YData',y);
            set(data.dragHandles(1),'XData',x);

            % Move the lines
            yData = get(data.boxHandles(1),'yData');
            set(data.boxHandles(1),'xData',[1 1]*x);
            set(data.boxHandles([1,2]),'yData',[yData(1) y]);
            xData = get(data.boxHandles(4),'xData');
            set(data.boxHandles(3),'yData',[1 1]*y);
            set(data.boxHandles([3,4]),'xData',[x xData(2)]);

        case 'UR'
            % Move the other two points
            set(data.dragHandles(3),'YData',y);
            set(data.dragHandles(2),'XData',x);

            % Move the lines
            yData = get(data.boxHandles(1),'yData');
            set(data.boxHandles(2),'xData',[1 1]*x);
            set(data.boxHandles([1,2]),'yData',[yData(1) y]);
            xData = get(data.boxHandles(4),'xData');
            set(data.boxHandles(3),'yData',[1 1]*y);
            set(data.boxHandles([3,4]),'xData',[xData(1) x]);

    end

    % Move the drag point
    set(obj,'XData',x);
    set(obj,'YData',y);

end


% Store out new drag point
data.dragStartPoint = curPt;

set(data.boxHandles,'UserData',data);
set(data.dragHandles,'UserData',data);

UpdateZoom(data);

%-------------------------------------------------------------------
% BoxDelete: Handles zoom box deletion.
%-------------------------------------------------------------------
function BoxDelete(han,varargin)
data = get(han,'Userdata');
if isempty(data), return; end

% First clear the delete fcns, then do the deletion.
try
    set(data.boxHandles,'DeleteFcn','');
    set(data.dragHandles,'DeleteFcn','');

    delete(data.boxHandles);
    delete(data.dragHandles);
end

% If zoom fig axes are attached, delete the figure
try
    delete(data.zoomFig);
end

%-------------------------------------------------------------------
% UpdateZoom: Updates the axes in the zoom figure
%-------------------------------------------------------------------
function UpdateZoom(data)

% REVISIT - Seems like we could do this faster if we passed a list of axes
% for type of zoom.
dragAxHan = gca;

% Control the standard figure
xlim = get(data.boxHandles(3),'XData');
ylim = get(data.boxHandles(1),'YData');

% Control any additional figures
if ~isempty(data.attachedAxes)
    nAxes = length(data.attachedAxes);
    for i = 1:nAxes,
        switch data.attachedAxes(i).limits,
            case 'x'
                set(data.attachedAxes(i).handle,'xLim',xlim);
            case 'y'
                set(data.attachedAxes(i).handle,'yLim',ylim);
            case 'xy'
                set(data.attachedAxes(i).handle,'xLim',xlim,'yLim',ylim);
        end
    end

    % PSD Update if exists.
    lineHan    = findobj(get(data.attachedAxes(i).handle,'Children'),'Type','line');
    if ~isempty(lineHan),
        axHan = getappdata(lineHan(1),'PSDAxHan');
        if ishandle(axHan),
            psdplot(data.attachedAxes(i).handle,axHan);
            axes(dragAxHan);
        end
    end

end


%-------------------------------------------------------------------
% AttachAxes: Adds additional axes to zoom limit control
%-------------------------------------------------------------------
function AttachAxes(varargin)

% Need to build a list of all other figure objects that are visible to us.
% Store out their ButtonDownFcn's, and put in a fcn to register as the
% selected figure.
% After user selects the figure, register it.
% Then restore the ButtonDownFcn's to the others.

% Get our data
data = get(gco,'UserData');
data.attachedAxesLimits = get(gcbo,'Tag');

% Build list of possible figures
allFigs = get(0,'Children');
ourFigs = data.origFig;
for i = 1:length(data.attachedAxes),
    ourFigs = [ourFigs get(data.attachedAxes(i).handle,'parent')];
end
testFigs = setdiff(allFigs,ourFigs);

testFigs = allFigs;

% If no figures, bail.
if isempty(testFigs)
    disp('No unmanaged figures available.');
    return; 
end


% Bring all candidates to the top, store out ButtonDownFcn, and replace
% with ours
nFigs = length(testFigs);
warning('Overwriting axes'' ButtonDownFcn without saving.');
disp('Select axes to manage.');
for i = 1:nFigs,
    %figure(testFigs(i));  % Bring up figure
    children = get(testFigs(i),'Children');
    children = findobj(children,'Type','Axes');
    set(children,'ButtonDownFcn',@RegisterAxes,'UserData',data);
end


%-------------------------------------------------------------------
% RegisterAxes: Callback to actually register the axes
%-------------------------------------------------------------------
function RegisterAxes(varargin)

% Not being very careful with other's data.
% I think I can use a user property or appdata to store my own unique
% field.
% Also need to modify the axes delete fcn to take it off of the list on
% deletion.
axHan = gca;
data = get(axHan,'UserData');

% Don't register ourselves.
if axHan == data.origAx
    return;
end

set(axHan,'ButtonDownFcn','','UserData',[]);

if ~isempty(data.attachedAxes),
    axNum = length(data.attachedAxes) + 1;
else
    axNum = 1;
end

data.attachedAxes(axNum).handle = axHan;
data.attachedAxes(axNum).limits = data.attachedAxesLimits;
data.attachedAxesLimits = '';
set([data.dragHandles data.boxHandles],'UserData',data);

% Set up figure for fast plotting
newFig = get(gca,'Parent');
set(newFig,'DoubleBuffer','on')
if strcmp(get(gcf,'renderer'),'painters'),
    set(newFig,'renderer','zbuffer');
end

% Unregister this axes set on deletion
for i = 1:length(data.attachedAxes),
    set(data.attachedAxes(i).handle,...
        'UserData',data,...
        'DeleteFcn',@UnregisterAxes);
end

% Force a zoom now
UpdateZoom(data);


%-------------------------------------------------------------------
% UnregisterAxes: Remove an axes handle from list of registered.
%-------------------------------------------------------------------
function UnregisterAxes(varargin)

handle = varargin{1};
data   = get(handle,'UserData');

% Remove specified axes handle from list
ind = [];
for i = 1:length(data.attachedAxes),
    if data.attachedAxes(i).handle == handle,
        ind = i;
    end
end
if ~isempty(ind),
    % Special handling of zoomfig
    if strcmp(get(handle,'BeingDeleted'),'on')
        if get(handle,'Parent') == data.zoomFig,
            data.zoomFig = [];
        end
    end

    data.attachedAxes(ind) = [];
end

% If no attached axes left, delete the zoom box
if isempty(data.attachedAxes)
    BoxDelete(handle);
    return;
end

% Update everyone
try %#ok<TRYNC>
    set([data.dragHandles data.boxHandles],'UserData',data); 
end
for i = 1:length(data.attachedAxes)
    try
        set(data.attachedAxes(i).handle,'UserData',data);
    end
end

%-------------------------------------------------------------
% Range
% Sub function to calc range without Stats Toolbox
%-------------------------------------------------------------
function [out] = Range(data)

out = max(data)-min(data);

