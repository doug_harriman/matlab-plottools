%PUSHAXIS  Stores current axis limits for all axes in the current figure.
%

% DLH
% 09/26/01 - Created

% Improvements:
% Make the axis stack a per axes property.

function [limits] = pushaxis(msg,varargin)

persistent AxisStack

% If no args, then assume store
if nargin == 0,
    msg = 'Store';
end

% Default to current figure
if nargin > 1
    figure(varargin{1});
end

switch lower(msg)
    case 'store'
        % Store out axis limits
        axesHans = gca; %findobj(get(figHan,'Children'),'Type','Axes');
        for i = 1:length(axesHans),
            axisLimits{i}.x = get(axesHans(i),'xlim');
            axisLimits{i}.y = get(axesHans(i),'ylim');
            axisLimits{i}.z = get(axesHans(i),'zlim');
        end

        % Make sure axis stack is a cell array
        if isempty(AxisStack),
            AxisStack = {};
        end

        % Push onto stack
        AxisStack{length(AxisStack)+1} = axisLimits;

    case 'retrieve'
        % Pop axis limits off stack and return
        if ~isempty(AxisStack),
            limits = AxisStack{end};
        else
            limits = [];
            return;
        end

        if length(AxisStack) > 1,
            AxisStack = AxisStack{1:end-1};
        else
            AxisStack = [];
        end

    otherwise
        error(['Unknown message: "' msg '"']);
end