%HISTLEGEND  Creates a legend with statistical information.
%  TXT_HAN = HISTLEGEND(DATA,AXES_HANDLE) creates a legend 
%  in AXES_HANDLE containing the mean, standard deviation 
%  and number of samples of the data vector DATA.  If 
%  AXES_HANDLE is not specified, the legend will be created 
%  in the current axes.  TXT_HAN is the handle of the text
%  object created.
%
%  HISTLEGEND OFF deletes the histlegend from the current figure.
%
%  Written by: Doug Harriman (doug_harriman@comcast.net)
%

% DLH
% 05/18/04 - Created.

function [txtHan] = histlegend(data,axHan)

% Error check inputs
narginchk(0,2);
if nargin < 1
    % Grab data from first axis child.
    warning('Pulling data from current axes line objects.');
    ch = get(gca,'Children');
    ch = findobj(ch,'Type','Line');
    if length(ch)>1
        warning('Multiple lines exist, choosing first one.');
        ch = ch(1);
    end
    
    data = get(ch,'YData');
end
    
if nargin < 2
    axHan = gca; 
end

if ~ishandle(axHan)
    error('Invalid axis handle.'); 
end
if length(axHan) > 1
    error('Axes handle value must be scalar.'); 
end
if min(size(data)) ~= 1
    error('data input must be 1xN or Nx1.'); 
end

try
    data = double(data);
end

if ~isnumeric(data),
    if ischar(data),
        if strcmpi(data,'off'),
            % Delete any existing histlegend    
            txtHan = findobj(get(axHan,'Children'),'Tag','histlegend');
            if ~isempty(txtHan),
                delete(txtHan);
            end
            clear txtHan
            return;    
        end
    end
    % If not numeric and got here, then something's awry.
    error('data input must be numeric.'); 
end

% Create text string
str = { [' Mean = ' num2str(mean(data)) ' '],...
        [' \sigma_x = ' num2str(std(data)) ' '],...
        [' Median = ' num2str(median(data)) ' '],...
        [' Mode = ' num2str(mode(data)) ' '],...
        [' Max = ' num2str(max(data)) ' '],...
        [' Min = ' num2str(min(data)) ' '],...
        [' Range = ' num2str(Range(data)) ' '],...
        [' N = ' num2str(length(data)) ' ']};

% Figure out where to place text
xLim = get(axHan,'xLim');
yLim = get(axHan,'yLim');

x = min(xLim) + 0.7*Range(xLim);
y = min(yLim) + 0.85*Range(yLim);

txtHan = text(x,y,str,...
    'Tag','histlegend',...
    'BackgroundColor','w',...
    'EdgeColor','k',...
    'ButtonDownFcn',@HistLegendButtonDownFcn);

% Only provide output if requested
if nargout == 0,
    clear txtHan
end


%-------------------------------------------------------------
% HistLegendButtonDownFcn
% Start drag of hist legend position
%-------------------------------------------------------------
function [] = HistLegendButtonDownFcn(han,eventdata,varargin)

% Store out current fcns
data.WindowButtonMotionFcn = get(gcf,'WindowButtonMotionFcn');
data.WindowButtonUpFcn     = get(gcf,'WindowButtonUpFcn');

% Set figure fcns temporarily
set(gcf,'WindowButtonMotionFcn',@HistLegendDragFcn) 
set(gcf,'WindowButtonUpFcn',@HistLegendButtonUpFcn)
pt = get(gca,'CurrentPoint');
pt = pt(1,1:2);
data.dragStartPoint = pt;
set(gco,'UserData',data);

%-------------------------------------------------------------
% HistLegendButtonUpFcn
% Start drag of hist legend position
%-------------------------------------------------------------
function [] = HistLegendButtonUpFcn(han,eventdata,varargin)

% Restore old fcns and clear data
data = get(gco,'UserData');
set(gcf,'WindowButtonMotionFcn',data.WindowButtonMotionFcn);
set(gcf,'WindowButtonUpFcn',data.WindowButtonUpFcn);
set(gco,'UserData',[]);

%-------------------------------------------------------------
% HistLegendDragFcn
% Sub function to allow the legend to be manually placed.
%-------------------------------------------------------------
function [] = HistLegendDragFcn(han,eventdata,varargin)

% Do the drag
han = gco;

% Bail if not on a line.
if ~strcmp(get(han,'Type'),'text'),
    return;
end

% Current Point
axHan = gca;
point = get(axHan,'CurrentPoint');
curPt = point(1,1:2);

% Our data
data = get(han,'UserData');

% Shift
dPt = curPt - data.dragStartPoint;

% Do the move
pos = get(han,'Position');
pos = pos + [dPt 0];
set(han,'Position',pos);

% Update data
data.dragStartPoint = curPt;
set(han,'UserData',data);


%-------------------------------------------------------------
% Range
% Sub function to calc range without Stats Toolbox
%-------------------------------------------------------------
function [out] = Range(data)

out = max(data)-min(data);

