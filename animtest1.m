%ANIMTEST1  ANIMOBJ example/test script.
%   Demonstrates a custom animation function: animdraw.m.
%
%   See also: animobj, animdraw.
%

% Doug Harriman (doug.harriman@comcast.net)

% General animation 
animobj('NewAnimation');
animobj('SetTotalFrames',20);

% Create object to animate.
figure;
x = [0:.01:1];
y = exp(-5*x);
h = plot(x,y,'b');
set(gca,'xlim',[0 1]);
set(gca,'ylim',[0 1]);
hold on

% Animate the curve fade in, then remove the animation.
animobj('AddObject',h);
animobj('SetAnimateProperty',h,'Color',[1 1 1],[0 0 1]);
animobj('Animate');
animobj('RmObject',h);

% Draw lines in to a given point
x_val = 0.3;
test  = abs(x-x_val);
index_2 = find(test == min(test));
h2 = plot([1 1]*x_val,[1 1]*0,'r');

x_val = 0.1;
test  = abs(x-x_val);
index_3 = find(test == min(test));
h3 = plot([1 1]*x_val,[1 1]*0,'r');

animobj('AddObject',[h2 h3]);
animobj('SetAnimateProperty',h2,'YData',[0 0],[0 y(index_2)]);
animobj('SetAnimateProperty',h3,'YData',[0 0],[0 y(index_3)]);
animobj('Animate');
animobj('RmObject',[h2 h3]);

% Remove the initial curve point by point
animobj('SetTotalFrames',length(x)-1);
animobj('AddObject',h);
data.x = get(h,'XData');
data.y = get(h,'YData');
animobj('SetAnimateProperty',h,'Trace',data,[],'animdraw');
animobj('Animate');
animobj('RmObject',h);

% Remove the red lines
animobj('SetTotalFrames',10);
animobj('AddObject',[h2 h3]);
animobj('SetAnimateProperty',[h2 h3],'LineWidth',1,10);
animobj('SetAnimateProperty',[h2 h3],'Color',[1 0 0],[1 1 1]);
animobj('Animate')

