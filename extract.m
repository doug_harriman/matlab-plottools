%EXTRACT  Graphical selection of data.
%   Returns logical indecies of selected data for first line
%   object in current axes.
%
%   Example:
%   >> x = [1:100];
%   >> plot(x)
%   >> ind = extract;  % Graphically select some portion of the plot.
%   >> plot(x(ind))    % Plots just region that was selected.
%
%   Written by: Doug Harriman (doug_harriman@comcast.net)
%

function index = extract()

% Call up current figure
figure(gcf) ;
cur = get(gcf,'pointer') ;
set(gcf,'pointer','fullcrosshair') ;

% Disable any widowbutton down functions
set(gcf,'windowbuttondownfcn',' ') ;

% Get zoom box
disp('Select region')
waitforbuttonpress            ;
pnt = get(gcf,'currentpoint') ;
xy1 = get(gca,'currentpoint') ;
rbbox([pnt 0 0],pnt)          ;
xy2 = get(gca,'currentpoint') ;

% Reset pointer
set(gcf,'pointer',cur) ;

% Clean up data
xy1 = xy1(1,1:2) ;
xy2 = xy2(1,1:2) ;
xx  = [xy1(1) xy2(1)] ;
yy  = [xy1(2) xy2(2)] ;

% Get xdata
han   = get(gca,'children') ;
han   = findobj(han,'Type','line');
xdata = get(han(1),'xdata') ;

% Generate index
index = (xdata >= xx(1)) & (xdata <= xx(2)) ;
index = index' ;
% Reset cursor
