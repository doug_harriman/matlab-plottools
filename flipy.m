%FLIPY  Positive Y direction on specified axes.
%

% DLH
% 06/20/05 - Created

function [] = flipy(ax)

% Error check inputs
if nargin > 1, error('One input expected.'); end
if nargin == 0, ax = gca; end

dir = lower(get(ax,'YDir'));
switch(dir),
    case 'normal'
        new_dir = 'reverse';
    case 'reverse'
        new_dir = 'normal';
end
set(ax,'YDir',new_dir);
