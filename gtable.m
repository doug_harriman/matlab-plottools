%GTABLE  Table generated from graphical selection of plot data.
%   GTABLE generates a table of data from all lines in the current plot.
%   The user graphically selects a subregion of the plot from which to
%   generate the table.
%

% Doug Harriman (doug.harriman@comcast.net)
% 11/26/04 - Created

% To Do:
% - More output formats:
%   csv, tab delimited, etc.
% - Export directly into Excel.

function [outMat] = gtable()

% UI
display('Select region of plot for table data.');

% Get range selection
fig=gcf;
oldCur = get(fig,'Pointer');
set(fig,'Pointer','CrossHair');

% Let the user select the data.
% Next six lines from RBBOX help.
k = waitforbuttonpress;
point1 = get(gca,'CurrentPoint');    % button down detected
finalRect = rbbox;                   % return figure units
point2 = get(gca,'CurrentPoint');    % button up detected
point1 = point1(1,1:2);              % extract x and y
point2 = point2(1,1:2);

% Return the cursor
set(fig,'Pointer',oldCur);

% Figure out the x range of interest
xRange = sort([point1(1) point2(1)]);

% Get data sets
lineLst = findobj(get(gca,'Children'),'Type','Line');
lineLst = flipud(lineLst);  % Put back into order that lines were added.
nLine = length(lineLst);
xData = [];
yData = [];

% Build up list of all X points
for i = 1:nLine,
    iLine = lineLst(i);
    
    % Get only the X points we're interested in.
    tempXData = get(iLine,'xData');
    tempXData = tempXData([tempXData >= xRange(1)]);
    tempXData = tempXData([tempXData <= xRange(2)]);
    
    xData = [xData tempXData];
end
xData = sort(unique(xData))';

% Output data will be of form [X Y1 Y2 Y3 ...] where each var is a column
% vector.  If a particular Yn doesn't have a value for all X values, then
% we need to insert a NaN to represent no data.
outMat = [xData NaN*ones(length(xData),nLine)];

% Loop through each line replacing NaN's where we have Y data.
for i = 1:nLine,

    % Get the line data
    iLine = lineLst(i);
    x = get(iLine,'xData');
    y = get(iLine,'yData');
    
    % Extract the Y data points of interest.
    yInd = ismember(x,xData);
    y = y(yInd);  

    % Find out where they go into the outMat
    outInd = find(ismember(xData,x));
    
    % Put them in place
    outMat(outInd,i+1) = y;
end
assignin('base','GTable_Data',outMat);
openvar('GTable_Data');

% Get matrix column header strings
txt = get(get(gca,'xLabel'),'String');
if isempty(txt),
    header{1} = 'X Data';
else
    header{1} = txt;
end
% Y data
for i=1:nLine,
    % Get the line data
    iLine = lineLst(i);

    txt = get(iLine,'DisplayName');
    if isempty(txt),
        header{i+1} = ['Line ' num2str(i)];
    else
        header{i+1} = txt;
    end
end

% Turn header and data column vector into string column for display.
outStr = '';
blankCol = ' '*ones(size(outMat,1)+1,3);
for i = 1:length(header),
    lenHeader = length(header{i});
    dataStr   = num2str(outMat(:,i));
    lenData   = size(dataStr,2);
    
    % Get rid of NaN's
    dataStr(dataStr=='N') = ' ';
    dataStr(dataStr=='a') = ' ';
    
    % Create column of blanks
    lenBlanks = abs(lenHeader-lenData);
    if lenBlanks > 0,
        % Decide whether to add to header or numbers
        if lenHeader > lenData,
            blankArry = ones(size(dataStr,1),lenBlanks);
            blankArry = blankArry * ' ';
            dataStr = [blankArry dataStr];
        else
            blankArry = ones(1,lenBlanks)*' ';
            headerStr{i} = [headerStr{i} blankArry];
        end
    end
    
    % Put the header on top of the numbers.
    if i > 1
        outStr = [outStr blankCol [header{i}; dataStr]];
    else
        outStr = [outStr [header{i}; dataStr]];
    end
end

disp(outStr);