%
%  Plot Tools
%  Version 1.0  01-Jun-2004
%
%  Data Annotation and Analysis
%    pt          - Adds context menus to axes and line objects.
%    psdplot     - PSD of viewable contents of axes.
%    histlegend  - Adds statistical data legend to plot. 
%
%  Axes Limits Control
%    dzoom       - Dynamic zoom control of slave axes by zoom box in master axes. 
%    panplot     - Modify axes limits by click and drag.
%    pushaxis    - Places current axes limits on a stack.
%    popaxis     - Pops axes limits off of stack and sets current axes limits.
%    syncax      - Synchronize the axes limits.
%    stretch     - Sets axes x limits to extent of data.
% 
%  Graphical Data Selection
%    extract     - Returns indecies of selected plot data.
%    gdata       - Graphically select a region of a plot and show data table.
%
%  Recommend Commands
%  These commands are not included, but can be found on Matlab Central.
%  See http://www.mathworks.com/matlabcentral
%     dragplot   - Drag line objects from one figure to another.
%     dualcursor - Adds vertical lines for measurment of plotted data.
%